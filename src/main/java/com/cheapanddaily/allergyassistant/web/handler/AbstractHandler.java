package com.cheapanddaily.allergyassistant.web.handler;

import com.cheapanddaily.allergyassistant.config.Configuration;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractHandler implements Handler<RoutingContext> {
    protected Configuration configuration;

    public AbstractHandler(Configuration configuration) {
        this.configuration = configuration;
    }
}
