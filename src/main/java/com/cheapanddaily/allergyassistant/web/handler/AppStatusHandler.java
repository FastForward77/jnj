package com.cheapanddaily.allergyassistant.web.handler;

import com.cheapanddaily.allergyassistant.config.Configuration;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public class AppStatusHandler extends AbstractHandler {
    private static Logger logger = LoggerFactory.getLogger(AppStatusHandler.class);


    public AppStatusHandler(Configuration configuration) {
        super(configuration);
    }

    @Override
    public void handle(RoutingContext routingContext) {
        JsonObject json = new JsonObject();
        json.put("state", "running");

        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(200)
                .end(json.encodePrettily());


    }
}
