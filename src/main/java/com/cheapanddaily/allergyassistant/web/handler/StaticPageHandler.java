package com.cheapanddaily.allergyassistant.web.handler;

import com.cheapanddaily.allergyassistant.config.Configuration;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;

public class StaticPageHandler extends AbstractHandler {
    private ThymeleafTemplateEngine engine;
    private String path;

    public StaticPageHandler(Configuration conf, ThymeleafTemplateEngine engine, String path) {
        super(conf);
        this.engine = engine;
        this.path = path;
    }

    @Override
    public void handle(RoutingContext ctx) {
        engine.render(ctx, "templates", path, res -> {
            if (res.succeeded()) {
                ctx.response()
                        .putHeader("content-type", "text/html")
                        .end(res.result());
            } else {
                ctx.fail(res.cause());
            }
        });
    }
}
