package com.cheapanddaily.allergyassistant.web.verticle;

import com.cheapanddaily.allergyassistant.config.Configuration;
import com.cheapanddaily.allergyassistant.config.ConfigurationException;
import com.cheapanddaily.allergyassistant.config.InMemoryConfiguration;
import com.cheapanddaily.allergyassistant.web.handler.AppStatusHandler;
import com.cheapanddaily.allergyassistant.web.handler.StaticPageHandler;
import com.cheapanddaily.allergyassistant.web.handler.collecting.AllergiesCollectingHandler;
import com.google.common.collect.Maps;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;

import java.io.IOException;
import java.util.Properties;

import static io.vertx.core.http.HttpMethod.*;

public class HttpServerVerticle extends AbstractVerticle {
    private static Logger logger = LoggerFactory.getLogger(HttpServerVerticle.class);
    private Configuration config;

    private int vertxServerPort;

    private ThymeleafTemplateEngine engine;

    public HttpServerVerticle() {
        this.engine = ThymeleafTemplateEngine.create();
        config = loadConfigurationFromClasspath(HttpServerVerticle.class.getClassLoader(), "application.properties");
        vertxServerPort = config.get("http.port").map(Integer::parseInt).orElse(9095);
    }

    @Override
    public void start() throws Exception {
        super.start();
        HttpServer server = vertx.createHttpServer();
        server.requestHandler(router()::accept).listen(vertxServerPort, result -> {
            if (result.succeeded()) {
                logger.info("Vert.x server started.");
                logger.info("Vert.x port: " + vertxServerPort);

            } else {
                logger.error("Failed to start server.", result.cause());
                vertx.close();
            }
        });
    }

    private Router router() {
        Router router = Router.router(vertx);
        router.route().handler(CorsHandler.create("*")
                .allowedMethod(GET)
                .allowedMethod(POST)
                .allowedMethod(PUT)
                .allowedMethod(OPTIONS)
                .allowedHeader("Content-Type"));
        router.route("/*").handler(BodyHandler.create());

        router.route(GET, "/status").handler(new AppStatusHandler(config));
        router.route(GET, "/allergies").handler(new StaticPageHandler(config, engine, "/allergies.html"));

        router.route(GET, "/allergies-collect").handler(new AllergiesCollectingHandler(config));

        return router;
    }

    private Configuration loadConfigurationFromClasspath(ClassLoader classLoader, String propertyFile) {
        try {
            Properties properties = new Properties();
            properties.load(classLoader.getResourceAsStream(propertyFile));
            properties.putAll(System.getProperties());
            return new InMemoryConfiguration(Maps.fromProperties(properties));
        } catch (IOException e) {
            throw new ConfigurationException("Cannot load config file from classpath" + propertyFile, e);
        }
    }
}
