package com.cheapanddaily.allergyassistant.config;

import java.util.Optional;

public interface Configuration {
    Optional<String> get(String optionName);

    default String require(String optionName){
        return get(optionName).orElseThrow(() -> new ConfigurationException(optionName+" is missing"));
    }
}
