package com.cheapanddaily.allergyassistant.config;

import java.util.Map;
import java.util.Optional;

public class InMemoryConfiguration implements Configuration {
    private Map<String, String> configMap;

    public InMemoryConfiguration(Map<String, String> configMap) {
        this.configMap = configMap;
    }

    @Override
    public Optional<String> get(String optionName) {
        return Optional.ofNullable(configMap.get(optionName));
    }
}
