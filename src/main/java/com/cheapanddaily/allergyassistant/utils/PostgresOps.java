package com.cheapanddaily.allergyassistant.utils;

import com.cheapanddaily.allergyassistant.config.Configuration;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public enum  PostgresOps {
    DB;

    private static final Logger logger = LoggerFactory.getLogger(PostgresOps.class);

    public static final String POSTGRES_URL = "db.postgresql.url";
    public static final String POSTGRES_USERNAME = "db.postgresql.username";
    public static final String POSTGRES_PASSWORD = "db.postgresql.password";

    private BasicDataSource connectionPool;

    private void connect(Configuration conf) {

        String url = conf.require("db.postgresql.url");
        String username = conf.require("db.postgresql.username");
        String password = conf.require("db.postgresql.password");

        connectionPool = new BasicDataSource();

        connectionPool.setUsername(username);
        connectionPool.setPassword(password);

        connectionPool.setDriverClassName("org.postgresql.Driver");
        connectionPool.setUrl(url);
        connectionPool.setInitialSize(3);
    }

    public Connection getConnection(Configuration conf) throws SQLException {
        if (connectionPool == null) {
            connect(conf);
        }
        return connectionPool.getConnection();
    }
}
