package com.cheapanddaily.allergyassistant;

import com.cheapanddaily.allergyassistant.web.verticle.HttpServerVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class WebServer {

    public static void main(String[] args) {
        VertxOptions options = new VertxOptions();
        options.setBlockedThreadCheckInterval(1000 * 60 * 60l);
        System.setProperty ("vertx.disableDnsResolver", "true");

        final Vertx vertx = Vertx.vertx(options);
        vertx.deployVerticle(new HttpServerVerticle());
    }
}
