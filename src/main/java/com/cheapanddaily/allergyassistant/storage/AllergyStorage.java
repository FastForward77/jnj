package com.cheapanddaily.allergyassistant.storage;

import com.cheapanddaily.allergyassistant.storage.model.Allergy;

import java.util.List;

public interface AllergyStorage {
    List<Allergy> getUserAllergies(String userId);
    void saveAllergy(Allergy allergy);
}
